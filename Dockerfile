FROM python:3.7-alpine

ENV PYTHONUNBUFFERED 1
ENV PATH="/scripts:${PATH}"

COPY ./requirements.txt /requirements.txt
COPY ./app /app
COPY ./scripts /scripts

WORKDIR /app

RUN apk add --update --no-cache postgresql-client jpeg-dev && \
    apk add --update --no-cache --virtual .tmp-build-deps \
      gcc libc-dev linux-headers postgresql-dev musl-dev zlib zlib-dev && \
    pip install --upgrade pip && \
    pip install -r /requirements.txt && \
    apk del .tmp-build-deps && \
    chmod +x /scripts/* && \
    mkdir -p /vol/web/media && \
    mkdir -p /vol/web/static && \
    adduser -D user && \
    chown -R user:user /vol/ && \
    chmod -R 755 /vol/web

USER user

VOLUME /vol/web

CMD ["entrypoint.sh"]