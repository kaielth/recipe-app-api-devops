resource "aws_s3_bucket" "app_public_files" {
  bucket        = "${local.prefix}-django-app-user-media-files"
  acl           = "public-read"
  force_destroy = true
}